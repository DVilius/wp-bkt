<?php
/**
 * The template for displaying front page.
 */

get_header();

global $column_iteration;
?>

    <div class="front-content">
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <?php foreach ( parse_blocks( get_the_content(null, false, get_the_ID()) ) as $block) :
                    if ($block['blockName'] !== "acf/gallery" ) echo render_block($block);
                endforeach; ?>

            <?php endwhile; ?>
        <?php endif; ?>
    </div>

<?php
get_footer();