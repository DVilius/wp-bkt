<?php
/**
 * The template for displaying the footer.
 */
?>

<footer>
    <div class="wrap">
        <ul>
            <li class="contacts">
                <a class="phone" href="tel:<?php the_field('phone', 'option'); ?>">
                    <strong><?php the_field('phone', 'option'); ?></strong>
                </a>
                <a class="mail" href="mailto:<?php the_field('email', 'option'); ?>">
                    <?php the_field('email', 'option'); ?>
                </a>
                <?php if (ICL_LANGUAGE_CODE == 'ru') : ?>
                    <a class="address" target="_blank" href="https://www.google.com/maps/place/<?php the_field('address_ru', 'option'); ?>"><?php the_field('address_ru', 'option'); ?></a>
                <?php else : ?>
                    <a class="address" target="_blank" href="https://www.google.com/maps/place/<?php the_field('address', 'option'); ?>"><?php the_field('address', 'option'); ?></a>
                <?php endif; ?>
            </li>

            <?php if ( get_field('footer-youtube', 'option') ||  get_field('footer-linkedin', 'option') ) : ?>
                <li>
                    <ul class="socials">
                        <?php if ( get_field('footer-youtube', 'option') ) : ?>
                            <li class="social-yt">
                                <a href="<?php the_field('footer-youtube', 'option'); ?>" title="youtube" target="_blank">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/images/youtube.svg'?>"
                                         alt="Youtube"/>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ( get_field('footer-linkedin', 'option') ) : ?>
                            <li class="social-in">
                                <a href="<?php the_field('footer-linkedin', 'option'); ?>" title="LinkedIn" target="_blank">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/images/linkedin.svg'?>"
                                         alt="LinkedIn"/>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <li>
                <?php wp_nav_menu( array(
                    'depth'             => 1,
                    'theme_location'    => 'about_menu',
                    'container_class'   => 'about_menu',
                )); ?>
            </li>
            <li class="info-links">
                <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                    <?php the_field('footer-info-en', 'option'); ?>
                <?php elseif (ICL_LANGUAGE_CODE == 'ru') : ?>
                    <?php the_field('footer-info-ru', 'option'); ?>
                <?php else : ?>
                    <?php the_field('footer-info', 'option'); ?>
                <?php endif; ?>
            </li>
            <li class="information">
                <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                    <?php the_field('footer-info2-en', 'option'); ?>
                <?php elseif (ICL_LANGUAGE_CODE == 'ru') : ?>
                    <?php the_field('footer-info2-ru', 'option'); ?>
                <?php else : ?>
                    <?php the_field('footer-info2', 'option'); ?>
                <?php endif; ?>
                <p class="copyright"><?php _e('© 2020. UAB „Biriu kroviniu terminalas“', 'bkt'); ?></p>
            </li>
            <li class="cp">
                <p><a target="_blank" style="transform: translate(0px, -3px);" href="https://www.cpartner.lt/lt/interneto-svetainiu-kurimas/"
                      title="Interneto svetainiu kūrimas">
                        <?php _e('Sukurė', 'bkt'); ?>
                    </a>
                    <a target="_blank" href="https://www.cpartner.lt/">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/cp.svg'?>" alt="Creative Partner"/>
                    </a>
                </p>
            </li>
        </ul>
    </div>
</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
