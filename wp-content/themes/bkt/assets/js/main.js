(function () {

    let mobile_menu = document.getElementById('mobile-menu');
    if (typeof(mobile_menu) != 'undefined' && mobile_menu != null)
    {
        let mobileNavigation = document.getElementsByClassName('mobile-navigation')[0];
        mobile_menu.onclick = function (e) {
            mobileNavigation.classList.add('navigation-open');
        };

        let mobile_menu_close = document.getElementById('close-menu');
        if (typeof(mobile_menu_close) != 'undefined' && mobile_menu_close != null)
        {
            mobile_menu_close.onclick = function (e) {
                mobileNavigation.classList.remove('navigation-open');
            };
        }
    }

    let infoTabs = document.querySelectorAll('.tabs li');
    for (let i = 0; i < infoTabs.length; i++){
        infoTabs[i].addEventListener("click", function(e) {

            let index = infoTabs[i].getAttribute('data-tab-index');
            for (let a = 0; a < infoTabs.length; a++) {
                infoTabs[a].classList.remove('active');
            }

            infoTabs[i].classList.add('active');

            let tabContent = document.querySelectorAll('.tab-content li');
            for (let a = 0; a < tabContent.length; a++) {
                tabContent[a].classList.remove('tab-open');
            }

            for (let a = 0; a < tabContent.length; a++)
            {
                if ( index === tabContent[a].getAttribute('data-tab-index') ) {
                    tabContent[a].classList.add('tab-open');
                }
            }
        });
    }

    let pdfs_el = document.getElementById('pdfs');
    if (typeof(pdfs_el) != 'undefined' && pdfs_el != null)
    {
        let totalFiles = pdfs_el.getAttribute("data-files");
        if (totalFiles > 5) {
            let pdfs = new Glide('#pdfs', {
                type: 'slider',
                gap: 0,
                perView: 5,
                breakpoints: {
                    1280: {
                        perView: 4
                    },
                    1080: {
                        perView: 3
                    },
                    780: {
                        perView: 2
                    },
                    450: {
                        perView: 1
                    }
                }
            });

            let total = document.querySelectorAll('.pdfs-content ul li.single-pdf').length;
            let perView = pdfs.settings.perView;

            pdfs.on(['mount.after', 'run'], function()
            {
                let slideBack = document.querySelector('.image-slide-prev').classList;
                let slideNext = document.querySelector('.image-slide-next').classList;

                if ((total / perView).toFixed() == (pdfs.index / perView)) {
                    slideNext.add("inactive-slide-button");
                } else {
                    slideNext.remove("inactive-slide-button");
                }

                if (pdfs.index === 0) {
                    slideBack.add("inactive-slide-button");
                } else {
                    slideBack.remove("inactive-slide-button");
                }
            });

            pdfs.mount();
        }
    }

    let timeline_el = document.getElementById('timeline');
    if (typeof(timeline_el) != 'undefined' && timeline_el != null)
    {
        let timeline = new Glide('#timeline', {
            type: 'slider',
            gap: 0,
            perView: 5,
            touchRatio: 1,
            perTouch: 2,
            perSwipe: '|',
            breakpoints: {
                1280: {
                    perView: 4
                },
                1080: {
                    perView: 3
                },
                780: {
                    perView: 2
                },
                450: {
                    perView: 1
                }
            }
        });

        let timeline_total = document.querySelectorAll('.timeline ul li.timeline-slide').length;
        let perView = timeline.settings.perView;

        timeline.on(['mount.after', 'run'], function() {

            let slideBack = document.getElementById('image-slide-prev').classList;
            let slideNext = document.getElementById('image-slide-next').classList;

            if ((timeline_total / perView ).toFixed() == (timeline.index / perView )) {
                slideNext.add("inactive-slide-button");
            } else {
                slideNext.remove("inactive-slide-button");
            }

            if (timeline.index === 0) {
                slideBack.add("inactive-slide-button");
            } else {
                slideBack.remove("inactive-slide-button");
            }
        });

        timeline.mount();
    }

    let slideBack = document.getElementById('image-slide-prev');
    let slideNext = document.getElementById('image-slide-next');
    if (typeof(slideBack) != 'undefined' && slideBack != null
        && typeof(slideNext) != 'undefined' && slideNext != null) {

        slideNext.onclick = function (e) {
            slideLazyLoad();
        };
    }

    function slideLazyLoad()
    {
        lazy = document.getElementsByClassName('lazy-image');
        for (let i = 0; i < lazy.length; i++){
            if (lazy[i].getAttribute('data-src')){
                if (lazy[i].tagName === "IMG") {
                    lazy[i].src = lazy[i].getAttribute('data-src');
                    lazy[i].removeAttribute('data-src');

                } else {

                    lazy[i].style['background-image'] = 'url("' + lazy[i].getAttribute('data-src') + '")';
                    lazy[i].removeAttribute('data-src');
                }
            }
        }
    }

    let resize_images = true;
    let gallery = document.getElementById('gallery-slide');
    if (typeof(gallery) != 'undefined' && gallery != null)
    {
        let gallery = new Glide('#gallery-slide', {
            type: 'carousel',
            gap: 0,
            perView: 10,
            startAt: 0,
            bound: true,
            breakpoints: {
                2560: { perView: 8 },
                1920: { perView: 7 },
                1560: { perView: 6 },
                1360: { perView: 5 },
                1280: { perView: 4 },
                800: { perView: 3 },
                500: { perView: 2 },
                320: { perView: 1 },
            },
        });

        gallery.mount();

        function resize()
        {
            let images = document.querySelectorAll('.gallery-block img');
            for (let i = 0; i < images.length; i++){
                images[i].style.height = images[i].width + "px";
                images[i].classList.add('loaded');
            }
            resize_images = false;
        }

        window.addEventListener("resize", function(e) {
            resize();
        });

        window.addEventListener("scroll", function(e) {
            if ( resize_images ) resize();
        });
    }

    let carousel_el = document.getElementById('carousel');
    if (typeof(carousel_el) != 'undefined' && carousel_el != null)
    {
        let autoPlay = true;
        let carousel = new Glide('#carousel', {
            type: 'slider',
            gap: 0,
            perView: 1,
            hoverpause: false,
            autoplay: 5000,
        });

        let toggle = document.getElementById('slide-toggle');
        if (typeof(toggle) != 'undefined' && toggle != null)
        {
            toggle.addEventListener('click', function ()
            {
                autoPlay = !autoPlay;
                if (autoPlay) autoPlay = 5000;
                carousel.update({
                    autoplay: autoPlay
                });

                let toggleButton = document.getElementById('slide-toggle-button');
                if (typeof(toggleButton) != 'undefined' && toggleButton != null) {
                    autoPlay ? toggleButton.classList.remove("paused") : toggleButton.classList.add("paused");
                }
            });
        }

        carousel.mount();
    }

    let carousel_el2 = document.getElementById('carousel-2');
    if (typeof(carousel_el2) != 'undefined' && carousel_el2 != null)
    {
        let autoPlay2 = true;
        let carousel2 = new Glide('#carousel-2', {
            type: 'slider',
            gap: 0,
            perView: 1,
            hoverpause: false,
            autoplay: 5000,
        });

        let toggle = document.getElementById('slide-toggle2');
        if (typeof(toggle) != 'undefined' && toggle != null)
        {
            toggle.addEventListener('click', function ()
            {
                autoPlay2 = !autoPlay2;
                if (autoPlay2) autoPlay2 = 5000;
                carousel2.update({
                    autoplay: autoPlay2
                });

                let toggleButton2 = document.getElementById('slide-toggle-button2');
                autoPlay2 ? toggleButton2.classList.remove("paused") : toggleButton2.classList.add("paused");
            });
        }

        carousel2.mount();
    }

    if (typeof refreshFsLightbox === "function")
    {
        let gallery = document.querySelectorAll('.wp-block-gallery img');
        for (let i = 0; i < gallery.length; i++)
        {
            let src = gallery[i].getAttribute("data-link");
            let href = document.createElement('a');
            href.setAttribute("href", src);
            href.setAttribute("data-fslightbox", 'galerija');

            gallery[i].parentNode.insertBefore(href, gallery[i]);
            href.appendChild(gallery[i]);
        }
        refreshFsLightbox();
    }

    window.addEventListener('resize', function()
    {
        if (window.innerWidth > 800) {
            document.querySelector( '.header' ).classList.remove( 'mobile-header' );
        }

        if (window.innerWidth > 1300) {
            document.querySelector( '.mobile-navigation' ).classList.remove( 'navigation-open' );
        }
    });

    registerListener('load', animateBlocks);
    registerListener('scroll', animateBlocks);
    function animateBlocks()
    {
        let videoImage = document.getElementsByClassName('video-image')[0];
        if (typeof(videoImage) != 'undefined' && videoImage != null) {
            if (isInViewport(videoImage)) {
                videoImage.classList.add("video-image-animate");
            }
        }

        let blocks = document.getElementsByClassName('column-blocks-2');
        for (let i = 0; i < blocks.length; i++){
            if (isInViewport(blocks[i])) {
                blocks[i].classList.add("animate-block");
            }
        }

        let blocks2 = document.getElementsByClassName('column-block');
        for (let i = 0; i < blocks2.length; i++){
            if (isInViewport(blocks2[i])) {
                blocks2[i].classList.add("animate-block");
            }
        }

        let blocks3 = document.querySelectorAll('.inner-page .carousel-block');
        for (let i = 0; i < blocks3.length; i++){
            if (isInViewport(blocks3[i])) {
                blocks3[i].classList.add("animate-block2");
            }
        }

        let blocks4 = document.getElementsByClassName('column-block-3');
        for (let i = 0; i < blocks4.length; i++){
            if (isInViewport(blocks4[i])) {
                blocks4[i].classList.add("animate-block");
            }
        }
    }

    let lazy = [];
    registerListener('load', lazyLoad);
    registerListener('scroll', lazyLoad);
    registerListener('resize', lazyLoad);

    function registerListener(event, func) {
        if (window.addEventListener) {
            window.addEventListener(event, func)
        } else {
            window.attachEvent('on' + event, func)
        }
    }

    function lazyLoad(){

        lazy = document.getElementsByClassName('lazy-image');

        for (let i = 0; i < lazy.length; i++){
            if (isInViewport(lazy[i])) {

                if (lazy[i].getAttribute('data-src')){
                    if (lazy[i].tagName === "IMG") {

                        lazy[i].src = lazy[i].getAttribute('data-src');
                        lazy[i].removeAttribute('data-src');

                    } else {

                        lazy[i].style['background-image'] = 'url("' + lazy[i].getAttribute('data-src') + '")';
                        lazy[i].removeAttribute('data-src');
                    }
                }
            }
        }

        lazy = Array.prototype.filter.call(lazy, function(l){
            return l.getAttribute('data-src');
        });
    }

    function isInViewport(el){

        var rect = el.getBoundingClientRect();
        return (
            rect.top <= (window.innerHeight || document.documentElement.clientHeight)
        );
    }

    select("lang");
    select("companies");
    select("production");

    function select(selector) {

        let wrap, i, j, select, selectedItem, selectionWrap, items;
        wrap = document.getElementsByClassName(selector);

        for (i = 0; i < wrap.length; i++)
        {
            select = wrap[i].getElementsByTagName("select")[0];

            selectedItem = document.createElement("DIV");
            selectedItem.setAttribute("class", "select-selected");
            selectedItem.innerHTML = select.options[select.selectedIndex].innerHTML;

            wrap[i].appendChild(selectedItem);

            selectionWrap = document.createElement("DIV");
            selectionWrap.setAttribute("class", "select-items select-hide");

            for (j = 0; j < select.length; j++) {

                items = document.createElement("DIV");
                items.innerHTML = select.options[j].innerHTML;

                if (selector === "production") {
                    items.setAttribute('data-value', select.options[j].value);
                }
                if (select.options[j].selected) continue;

                items.addEventListener("click", function(e) {

                    let y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++)
                    {
                        if (s.options[i].innerHTML == this.innerHTML)
                        {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            if (selector === "production") {
                                h.setAttribute("data-value", this.getAttribute('data-value'));
                            }

                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++)
                            {
                                y[k].removeAttribute("class");
                            }

                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                    if (selector === "lang") {
                        window.location.href = 'http://' + window.location.hostname + '/' + h.textContent.toLowerCase();
                    }

                    if (selector === "production") {
                        let filter = { production: h.getAttribute("data-value") };
                        updateMarkers(filter);
                    }
                });

                selectionWrap.appendChild(items);
            }

            wrap[i].appendChild(selectionWrap);

            selectedItem.addEventListener("click", function(e) {

                e.stopPropagation();
                closeSelection(this);

                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }

        function closeSelection(elmnt) {

            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }
        document.addEventListener("click", closeSelection);
    }

})();