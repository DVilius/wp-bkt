<?php defined( 'ABSPATH' ) || exit; ?>

<?php if ( !is_single() ) : ?>
    <li class="single-post">
<?php endif; ?>

<article>

    <div class="thumbnail-wrap">
        <?php the_post_thumbnail(get_the_ID(), 'medium'); ?>
    </div>

    <?php if ( is_single()) : ?>

        <h3><?php the_title(); ?></h3>
        <?php the_content(); ?>

    <?php else : ?>

        <div class="content">
            <div class="meta">
                <?php echo get_the_date( 'M' ); ?><br />
                <span><?php echo get_the_date( 'd' ); ?></span><br />
                <?php echo get_the_date( 'Y' ); ?>
            </div>

            <div class="text-content">
                <a class="h3" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_title(); ?>
                </a>

                <div class="excerpt">
                    <?php the_excerpt(); ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>

    <?php endif; ?>
</article>

<?php if ( !is_single()) : ?>
    </li>
<?php endif; ?>