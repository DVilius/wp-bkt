<?php
/**
 * Columns template.
 */

$id = 'column-3-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'column-block-3';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
global $column_iteration;
$column_iteration++;
?>

</div> <!-- End of wrap -->

<div id="<?php echo esc_attr($id); ?>" data-iteration="<?php echo $column_iteration; ?>" class="<?php echo esc_attr($className); ?>">
    <div class="wrap">
        <div class="row">
            <div class="column column-1">
                <?php $image = get_field('column-block-image'); ?>
                <img class="lazy-image" src="" data-src="<?php echo $image ? $image['url'] : ''; ?>"
                     alt="<?php echo $image ? $image['title'] : ''; ?>" />
            </div>

            <div class="column column-2">
                <div class="content">
                    <div class="text-wrapper">

                        <h2><?php the_field('column-block-title'); ?></h2>
                        <?php if ( get_field('column-block-subtitle') ) : ?>
                            <h3><?php the_field('column-block-subtitle'); ?></h3>
                        <?php endif; ?>

                        <?php the_field('column-block-text'); ?>
                    </div>

                    <?php if ( have_rows('column-block-logos') ) : ?>
                        <div class="logos-wrapper">
                            <ul>
                                <?php while ( have_rows('column-block-logos') ) : the_row();

                                    $logo = get_sub_field('column-block-logos-logo');
                                    ?>
                                    <li>
                                        <div class="image-wrap">
                                            <img class="lazy-image"
                                                 src=""
                                                 data-src="<?php echo $logo ? $logo['url'] : ''; ?>" alt="" />
                                        </div>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

            <?php if ( have_rows('column-block-info') ) : ?>
                <div class="column column-3">
                    <div class="content">
                        <ul>
                            <?php while ( have_rows('column-block-info') ) : the_row();

                                $logo = get_sub_field('column-block-info-logo');
                                ?>
                                <li data-alt="<?php echo $logo ? $logo['alt'] : ''; ?>">
                                    <?php echo preg_replace('~^\D*\K(\d*\,\d|\d*)~m',
                                        '<span>$0</span>',
                                        get_sub_field('column-block-info-text')
                                    );?>
                                    <img src="<?php echo $logo ? $logo['url'] : ''; ?>" alt="" />
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>

<div class="wrap">