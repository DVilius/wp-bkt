<?php
/**
 * Partners template.
 */

$id = 'partners-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'partners-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="name">
        <h3><?php _e("Mūsų partneriai", "bkt"); ?></h3>
    </div>
    <div class="content">
        <?php if ( have_rows('partners') ) : ?>
            <ul>
                <?php while ( have_rows('partners') ) : the_row();

                $image = get_sub_field('partners-logo');
                $image_active = get_sub_field('partners-logo-active');
                $link = get_sub_field('partners-link');
                ?>

                    <li>
                        <a href="<?php echo $link ? $link['url'] : ''; ?>" target="_blank">
                            <img class="inactive-img lazy-image" src="" data-src="<?php echo $image ? $image['url'] : ''; ?>" alt="<?php echo $image ? $image['alt'] : ''; ?>" />
                            <img class="active-img" src="<?php echo $image_active ? $image_active['url'] : ''; ?>" alt="<?php echo $image ? $image['alt'] : ''; ?>" />
                        </a>
                    </li>

                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
    <div class="clear"></div>
</div>
