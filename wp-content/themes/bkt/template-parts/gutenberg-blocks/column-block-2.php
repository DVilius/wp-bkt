<?php
/**
 * Columns blocks 2 template.
 */

$id = 'column-2-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'column-blocks-2';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
global $column_iteration;
$column_iteration++;
?>

</div> <!-- End of wrap -->

<div id="<?php echo esc_attr($id); ?>" data-iteration="<?php echo $column_iteration; ?>" class="<?php echo esc_attr($className); ?>">
    <div class="wrap">
        <div class="row">
            <div class="column column-1">
                <?php $image = get_field('column-block-image'); ?>
                <img class="lazy-image" src="" data-src="<?php echo $image ? $image['url'] : ''; ?>"
                     alt="<?php echo $image ? $image['title'] : ''; ?>" />
            </div>

            <div class="column column-2">
                <div class="content">
                    <h2><?php the_field('column-block-title'); ?></h2>
                    <h3><?php the_field('column-block-subtitle'); ?></h3>
                    <?php the_field('column-block-text'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">