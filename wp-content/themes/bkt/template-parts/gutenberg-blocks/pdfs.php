<?php
/**
 * PDFs template.
 */

$files = count(get_field('pdf-files'));
?>

</div>

<div class="pdfs-block">
    <div class="wrap" id="pdfs" data-files="<?php echo $files; ?>">
        <?php if (get_field('pdf-block-title')) : ?>
            <div class="name">
                <h3><?php the_field('pdf-block-title'); ?></h3>
                <?php if ($files > 5) : ?>
                    <div data-glide-el="controls">
                        <button class="image-slide-prev" data-glide-dir="|<">
                            <span></span>
                        </button>
                        <button class="image-slide-next" data-glide-dir="|&gt;">
                            <span></span>
                        </button>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="pdfs-content content" data-glide-el="track">
            <?php if ( have_rows('pdf-files') ) : ?>
                <ul>
                    <?php while ( have_rows('pdf-files') ) : the_row();

                    $pdf = get_template_directory_uri() . '/assets/images/pdf.svg';
                    $link = get_sub_field('pdf-link');
                    ?>
                        <li class="single-pdf">
                            <a href="<?php echo $link ? $link['url'] : ''; ?>" target="_blank">
                                <img class="lazy-image" src="" data-src="<?php echo $pdf; ?>" alt="" />
                                <p><?php the_sub_field('pdf-file-name'); ?></p>
                            </a>
                        </li>

                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="wrap">
