<?php
/**
 * Terminal block template.
 */

$id = 'terminal-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'terminal-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$terminaImage = get_field('terminal-image');

?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <div class="terminal" >

        <div class="terminal-image">
            <?php
                echo wp_get_attachment_image($terminaImage, 'full');
            ?>
        </div>

        <div class="terminal-info">
            <ul>
                <?php
                    while ( have_rows('terminal') ) :
                        the_row();
                        $title = get_sub_field('terminal-title');
                        $desc = get_sub_field('terminal-desc');
                ?>
                     <li>
                         <h2><?php echo str_replace('</br>', '&#10;', $title); ?></h2>
                         <p><?php echo $desc; ?></p>
                     </li>
                <?php endwhile; ?>
            </ul>
        </div>

    </div>

</div>