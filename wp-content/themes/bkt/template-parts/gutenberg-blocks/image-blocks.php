<?php
/**
 * Image blocks template.
 */

$id = 'image-block-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'image-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

?>

</div> <!-- End of wrap -->

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="wrap">
        <?php
        $i = 0;
        if ( have_rows('image-blocks') ) : ?>
            <ul>
                <?php while ( have_rows('image-blocks') ) : the_row();

                $image = get_sub_field('image-block-image');
                $link = get_sub_field('image-block-link');
                ?>

                    <li>
                        <a href="<?php echo $link ? $link['url'] : ''; ?>">
                            <div class="content-wrap">
                                <div class="image-wrap">
                                    <div class="image lazy-image" data-src="<?php echo $image ? $image['url'] : ''; ?>"></div>
                                    <div class="title">
                                        <h4><?php the_sub_field('image-block-subtitle'); ?></h4>
                                        <h3><?php the_sub_field('image-block-title'); ?></h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>

                <?php
                    $i++;
                endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>

<div class="wrap">