<?php
/**
 * Timeline block template.
 */

$id = 'timeline-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'timeline-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
} ?>

</div>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <div class="wrap">
        <div class="timeline" id="timeline">

            <div class="controls">
                <h3><?php _e("Istorija", "bkt"); ?></h3>

                <div data-glide-el="controls">
                    <button id="image-slide-prev" data-glide-dir="|<">
                        <span></span>
                    </button>
                    <button id="image-slide-next" data-glide-dir="|&gt;">
                        <span></span>
                    </button>
                </div>
            </div>

            <div class="timeline-track" data-glide-el="track">
                <ul class="timeline-wrap">
                    <?php
                    $i = 0;
                    if ( have_rows('timeline') ) :
                        while ( have_rows('timeline') ) : the_row();

                        $year = get_sub_field('timeline-year');
                        $image = get_sub_field('timeline-image')
                        ?>
                            <li class="timeline-slide timeline-slide-<?php echo $i; ?>">
                                <div class="wrap-content">

                                    <div class="timeline-content">
                                        <img <?php echo !$image ? 'style="opacity: 0"' : ''; ?> src="<?php echo $image ? $image['url'] : ''; ?>" alt="" />
                                        <p><?php the_sub_field('timeline-title'); ?></p>
                                    </div>

                                    <ul class="time">
                                        <li></li>
                                        <li class="year<?php echo $i == 0 ? ' first-year' : ''; ?>"><h5><?php echo $year; ?></h5></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>

                                        <li></li>
                                        <li></li>
                                        <li></li>

                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </li>

                        <?php
                        $i++;
                        endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>

</div>

<div class="wrap">