<?php
/**
 * Contacts template.
 */

$id = 'contacts-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'contacts-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <div class="map-wrap">
        <div class="map" id="map" style="height: 600px;"></div>
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
              integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
              crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
                integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
                crossorigin=""></script>
        <script>
            let map = L.map('map', {
                scrollWheelZoom: false
            }).setView([55.6674057,21.1497014], 13);
            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGVtb3ZpbGl1cyIsImEiOiJjam5zeTJsaWswM2UxM3JvMXppamxkeXl2In0.gSBxj7rBcrJANrH_IzleiA', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'demovilius/ck8obixqd28pw1iqj142y8b8g',
                tileSize: 512,
                zoomOffset: -1,
            }).addTo(map);
            let icon = L.icon({
                iconUrl: '<?php echo get_template_directory_uri() . '/assets/images/marker.svg'; ?>',
                iconSize:     [120, 120],
                popupAnchor:  [-3, -76]
            });
            L.marker([55.6674057,21.1497014], {icon: icon}).addTo(map);
        </script>
    </div>

    <div class="wrap">
        <div class="row">

            <div class="column-1">
                <p>
                    <a class="phone" href="tel:<?php the_field('phone', 'option'); ?>">
                        <strong><?php the_field('phone', 'option'); ?></strong>
                    </a>
                </p>

                <p>
                    <a class="mail" href="mailto:<?php the_field('email', 'option'); ?>">
                        <?php the_field('email', 'option'); ?>
                    </a>
                </p>

                <p class="work-hours"><?php the_field('contacts-work-hours'); ?></p>

                <?php the_field('contacts-text'); ?>
            </div>

            <div class="column-2">
                <div class="contact-form">
                    <?php echo do_shortcode(the_field('contacts-form')); ?>
                </div>
            </div>

        </div>

    </div>

</div>
