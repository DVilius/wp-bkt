<?php
/**
 * Carousel block template.
 */

$id = 'carousel-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'carousel-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$slides = count(get_field('carousel'));

global $carousel_iteration;
$carousel_iteration++;

$slides = count(get_field('carousel'));
?>

<?php if ( is_page() && !is_front_page() ) : ?>
    </div> <!-- End of wrap-->
<?php endif; ?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <div class="carousel" id="<?php echo $carousel_iteration > 1 ? 'carousel-2' : 'carousel'; ?>">
        <?php if ( !is_front_page()  ) : ?>

            <?php if ( $slides > 1 ) : ?>
                <div class="slide-arrows" data-glide-el="controls" style="position: absolute">
                    <button data-glide-dir="<"></button>
                    <button data-glide-dir="&gt;"></button>
                </div>

                <div class="pause-slide">
                    <button id="<?php echo $carousel_iteration > 1 ? 'slide-toggle-button2' : 'slide-toggle-button'; ?>"></button>
                </div>
            <?php endif; ?>

        <?php endif; ?>

        <div class="carousel-track" data-glide-el="track" id="<?php echo $carousel_iteration > 1 ? 'slide-toggle2' : 'slide-toggle'; ?>">
            <ul class="carousel-wrap">
                <?php if ( have_rows('carousel') ) :
                    while ( have_rows('carousel') ) : the_row();

                        $background = get_sub_field('carousel-background');
                        $link = get_sub_field('carousel-link');
                        $video = get_sub_field('iframe-video');
                        $video_local = get_sub_field('local-video');
                        ?>

                        <?php if ( is_front_page() && $video ) : ?>

                            <li class="carousel-slide video">
                                <div class="slide-background"></div>
                                <div class="wrap">

                                    <?php if ( !$video_local ) : ?>
                                        <iframe id="ytplayer"
                                                type="text/html"
                                                width="100%"
                                                height="1045"
                                                src="<?php echo $video; ?>?autoplay=1&controls=0&disablekb=1&loop=1&playlist=wQ6gZ1QryNs&showinfo=0&modestbranding=1&mute=1"
                                                frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                    <?php else : ?>
                                        <video id="main-video" autoplay muted loop playsinline preload="auto">
                                            <source src="<?php echo $video_local; ?>" type="video/mp4">
                                        </video>
                                        <style>
                                            #main-video {
                                                box-sizing: border-box;
                                                height: 56.25vw;
                                                left: 50%;
                                                min-height: 100%;
                                                min-width: 100%;
                                                transform: translate(-50%, -50%);
                                                position: absolute;
                                                top: 50%;
                                                width: 177.77777778vh;
                                            }
                                        </style>
                                    <?php endif; ?>

                                    <div class="mobile-image" style="background-image: url(<?php echo $background ? $background['url'] : ''; ?>)"></div>

                                    <div class="mobile-video">
                                        <video id="mobile-video" autoplay muted loop playsinline preload="auto">
                                            <source src="<?php the_field('mobile_video', 'option'); ?>" type="video/mp4">
                                        </video>
                                        <script>
                                            var video = document.getElementById("mobile-video");
                                            video.pause();
                                            video.play();

                                            var videomain = document.getElementById("main-video");
                                            videomain.pause();
                                            videomain.play();
                                        </script>
                                    </div>

                                    <div class="wrap-content">
                                        <h3><?php the_sub_field('carousel-number'); ?></h3>
                                        <h2 class="h1"><?php the_sub_field('carousel-title'); ?></h2>
                                        <div class="text">
                                            <?php the_sub_field('carousel-text'); ?>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        <?php else : ?>
                            <li class="carousel-slide" style="background-image: url(<?php echo $background ? $background['url'] : ''; ?>)">
                                <div class="slide-background"></div>

                                <div class="wrap">
                                    <div class="wrap-content">

                                        <?php if (get_sub_field('carousel-number')) : ?>
                                            <h3><?php the_sub_field('carousel-number'); ?></h3>
                                            <h2 class="h1"><?php the_sub_field('carousel-title'); ?></h2>
                                        <?php else : ?>
                                            <h2 class="slider-title h1"><?php echo str_replace("</br>", "&#10;", get_sub_field('carousel-title')); ?></h2>
                                        <?php endif; ?>

                                        <div class="text">
                                            <?php the_sub_field('carousel-text'); ?>
                                            <?php if ( $video ) : ?>
                                                <div class="video-frame">
                                                    <iframe width="100%"
                                                            height="1045"
                                                            src="<?php echo $video; ?>?autoplay=0&controls=0&disablekb=1&showinfo=0&modestbranding=1"
                                                            frameborder="0"></iframe>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                        <?php if ($link) : ?>
                                            <a href="<?php echo $link ? $link['url'] : ''; ?>"
                                               title="<?php echo $link ? $link['title'] : ''; ?>" class="btn arrow-link">
                                                <?php echo $link ? $link['title'] : ''; ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>

                                    <?php if ($slides > 1) : ?>
                                        <div class="glide__bullets" data-glide-el="controls[nav]">
                                            <?php for ($i = 0; $i < $slides; $i++) : ?>
                                                <button class="glide__bullet" data-glide-dir="=<?php echo $i; ?>"></button>
                                            <?php endfor; ?>
                                        </div>
                                    <?php endif; ?>

                                </div>
                            </li>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>

</div>

<?php if ( is_page() && !is_front_page() ) : ?>
    <div class="wrap">
<?php endif; ?>