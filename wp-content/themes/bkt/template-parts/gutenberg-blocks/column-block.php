<?php
/**
 * Columns template.
 */

$id = 'column-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'column-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

?>

</div> <!-- End of wrap -->

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="wrap">
        <div class="row">
            <div class="column column-1">
                <div class="content">
                    <h2><?php the_field('column-block-title'); ?></h2>

                    <div class="logos">
                        <?php if ( have_rows('column-block-logos') ) :
                            while ( have_rows('column-block-logos') ) : the_row();

                                $image = get_sub_field('column-block-logos-logo');
                                ?>

                                <img src="<?php echo $image ? $image['url'] : ''; ?>" alt="" />

                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="text">
                        <?php the_field('column-block-text'); ?>
                    </div>
                </div>
            </div>
            <div class="column column-2">
                <?php $image = get_field('column-block-image'); ?>
                <img src="<?php echo $image ? $image['url'] : ''; ?>"
                     alt="<?php echo $image ? $image['url'] : ''; ?>" />
            </div>
        </div>
    </div>
</div>

<div class="wrap">