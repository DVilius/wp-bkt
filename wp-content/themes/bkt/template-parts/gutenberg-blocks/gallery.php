<?php
/**
 * Gallery block template.
 */

$id = 'gallery-slide-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'gallery-slide-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <div id="gallery-slide" class="gallery-slide">
        <div class="gallery-slide-track" data-glide-el="track">
            <?php
            $images = get_field('gallery-slider');
            $i = 0;
            if( $images ): ?>
                <ul class="gallery-slide-wrap">
                    <?php foreach( $images as $image_id ) : ?>

                    <?php if ($i % 3 == 0) : ?>

                    <li class="gallery-block">
                        <div class="gallery-block-img--big">
                            <?php echo wp_get_attachment_image( $image_id, 'medium' ); ?>
                            <div class="shadow"></div>
                            <a class="link" data-lightbox="gallery" href="<?php echo wp_get_attachment_url($image_id); ?>" target="_blank" rel="noopener"></a>
                        </div>
                    <?php else : ?>
                        <div class="gallery-block-img">
                            <?php echo wp_get_attachment_image( $image_id, 'thumbnail' ); ?>
                            <div class="shadow"></div>
                            <a class="link" data-lightbox="gallery" href="<?php echo wp_get_attachment_url($image_id); ?>" target="_blank" rel="noopener"></a>
                        </div>
                    <?php endif; ?>

                    <?php if ($i % 3 == 2) echo '</li>'; ?>
                    <?php $i++; ?>

                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>

        <div class="gallery-slide-controls" data-glide-el="controls">
            <button id="gallery-slide-prev" data-glide-dir="<"></button>
            <button id="gallery-slide-next" data-glide-dir=">"></button>
        </div>
    </div>

</div>
