<?php
/**
 * 2 Columns template.
 */

$id = '2-columns';
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'columns-block-2';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

?>

</div> <!-- End of wrap -->

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="wrap">
        <div class="row">
            <div class="column column-1">
                <div class="content">
                    <div class="video-image">
                        <a href="<?php the_field('2-column-video-link'); ?>" target="_blank">
                            <div class="play"></div>
                            <p><?php _e('Žiūrėti video', 'bkt');?></p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="column column-2">
                <div class="content">
                    <?php if (ICL_LANGUAGE_CODE != 'ru') : ?>
                        <h1><?php the_field('2-column-title'); ?></h1>
                    <?php endif; ?>
                    <?php the_field('2-column-text'); ?>
                </div>
            </div>
        </div>

        <?php if (ICL_LANGUAGE_CODE == 'ru') : ?>
            <h1 class="full"><?php the_field('2-column-title'); ?></h1>
        <?php endif; ?>
    </div>
</div>

<div class="wrap">