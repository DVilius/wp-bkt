<?php
/**
 * Statistics block template.
 */

$id = 'statistics-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'statistics-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$statisticsTitle = get_field('statistics_title');
$statisticsText = get_field('statistics_text');
$subtitle = get_field('subtitle');

?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <div class="statistics" >

        <div class="statistics-chart">

            <div class="statistics-info" >
                <h2>
                    <?php echo $statisticsTitle; ?>
                </h2>
                <?php echo $statisticsText; ?>
            </div>

            <?php
                $labels = '';
                $data = '';
                if ( have_rows('statistics') ) :
                    while ( have_rows('statistics') ) :
                        the_row();
                        $labels .= "'".get_sub_field('statistics-year')."',";
                        $data .= get_sub_field('statistics-viso').',';
                    endwhile;
                endif;
            ?>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
            <canvas id="statistics-chart"></canvas>
            <script>

                var t = 1e3,
                    e = 1e3,
                    i = 9,
                    s = 20,
                    n = 16,
                    o = 15;
                var a = document.getElementById("statistics-chart").getContext("2d"), l = a.createLinearGradient(0, 0, t, e);
                l.addColorStop(0, "transparent"), l.addColorStop(.1, "transparent"), l.addColorStop(1, "rgba(0, 0, 0, 0.03)");
                var d = a.createLinearGradient(0, 0, 0, t);
                d.addColorStop(0, "rgba(80,110,190,0.2)"), d.addColorStop(.5, "rgba(80,110,190,0.03)"), d.addColorStop(1, "rgba(80,110,190,0)");

                var myChart = new Chart(a, {
                    type: 'line',
                    data: {
                        labels: [<?php echo $labels; ?>],
                        datasets: [{
                            label: '',
                            data: [<?php echo $data; ?>],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: !0,
                        maintainAspectRatio: !0,
                        aspectRatio: 1.652972972972973,
                        title: {
                            display: !1,
                            //text: c,
                            fontColor: "#506ebe",
                            fontSize: 23,
                            fontFamily: '"SF Compact Display", sans-serif',
                            fontStyle: "normal",
                            lineHeight: 1.217391304347826,
                            padding: 0
                        },
                        legend: {
                            display: !1
                        },
                        elements: {
                            line: {
                                tension: 0,
                                borderColor: "#506ebe",
                                borderWidth: 2,
                                backgroundColor: d,
                                shadowColor: "#000000",
                                shadowBlur: 20
                            },
                            point: {
                                backgroundColor: "#506ebe",
                                borderColor: "#ffffff",
                                hoverBorderColor: "#ffffff",
                                hoverBorderWidth: 3,
                                borderWidth: 3,
                                radius: 5,
                                hoverRadius: 6
                            }
                        },
                        animation: {
                            duration: 0
                        },
                        responsiveAnimationDuration: 0,
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    color: l,
                                    zeroLineColor: "transparent",
                                    drawBorder: !1,
                                    zeroLineWidth: 0,
                                },
                                ticks: {
                                    fontColor: "rgba(80,110,190,0.52)",
                                    fontSize: 12,
                                    fontFamily: '"SF Compact Display", sans-serif',
                                    fontStyle: "600",
                                    lineHeight: 1.25,
                                    autoSkip: !0,
                                    maxTicksLimit: s,
                                }
                            }],
                            yAxes: [{
                                gridLines: {
                                    color: l,
                                    zeroLineWidth: 0
                                },
                                position: "right",
                                ticks: {
                                    beginAtZero: !0,
                                    fontColor: "#506ebe",
                                    fontSize: n,
                                    fontFamily: '"SF Compact Display", sans-serif',
                                    fontStyle: "600",
                                    lineHeight: 1.25,
                                    callback: function(t, e, i) {

                                        if (t == 0) return null;

                                        <?php if (ICL_LANGUAGE_CODE == 'lt') : ?>
                                        return t.toString().replace("000", "").replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1 ") + " t\u016Bkst. t.";
                                        <?php elseif (ICL_LANGUAGE_CODE == 'en') : ?>
                                        return t.toString().replace("000", "").replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1 ") + " thous. t";
                                        <?php endif; ?>
                                        return t.toString().replace("000", "").replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1 ") + " \u0422\u044B\u0441. \u0422\u043E\u043D\u043D";
                                    }
                                }
                            }]
                        },
                        tooltips: {
                            callbacks: {
                                label: function(t, e) {

                                    <?php if (ICL_LANGUAGE_CODE == 'lt') : ?>
                                    return 'Viso:' + t.yLabel.toString().replace("000", "").replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1 ") + " t\u016Bkst. t.";
                                    <?php elseif (ICL_LANGUAGE_CODE == 'en') : ?>
                                    return 'Total:' + t.yLabel.toString().replace("000", "").replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1 ") + " thous. t";
                                    <?php endif; ?>
                                    return t.yLabel.toString().replace("000", "").replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1 ") + " \u0422\u044B\u0441. \u0422\u043E\u043D\u043D";
                                }
                            }
                        },
                    }
                });

            </script>
        </div>

        <div class="statistics-table">
            <h4>
                <?php echo $subtitle; ?>
            </h4>
            <?php if ( have_rows('statistics') ) : ?>
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <?php
                            while ( have_rows('statistics') ) : the_row();
                            $year = get_sub_field('statistics-year');

                                if ( get_sub_field('statistics-in-chart') ) : ?>
                                    <th>
                                        <?php echo $year; ?>
                                    </th>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="th">
                                <?php echo __('Į konteinerius','bkt'); ?>
                            </td>
                            <?php while ( have_rows('statistics') ) :
                                the_row();
                                $konteineriai = get_sub_field('statistics-konteineriai');

                                if ( get_sub_field('statistics-in-chart') ) : ?>
                                    <td>
                                        <?php echo number_format($konteineriai, 0, ',', ' '); ?>
                                    </td>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        </tr>
                        <tr>
                            <td class="th">
                                <?php echo __('Į laivus','bkt'); ?>
                            </td>
                            <?php while ( have_rows('statistics') ) :
                                the_row();
                                $laivai = get_sub_field('statistics-laivai');

                                if ( get_sub_field('statistics-in-chart') ) : ?>
                                    <td>
                                        <?php echo number_format($laivai, 0, ',', ' '); ?>
                                    </td>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        </tr>
                        <tr>
                            <td class="th">
                                <?php echo __('Iš viso','bkt'); ?>
                            </td>
                            <?php while ( have_rows('statistics') ) :
                                the_row();
                                $viso = get_sub_field('statistics-viso');
                                if ( get_sub_field('statistics-in-chart') ) : ?>
                                    <td>
                                        <?php echo number_format($viso, 0, ',', ' '); ?>
                                    </td>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        </tr>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>

    </div>

</div>