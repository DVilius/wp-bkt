<?php

require 'inc/gutenberg-functions.php';

/*
 * Add support for WordPress features.
 **/
add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );

function enqueue_theme_scripts()
{
    wp_enqueue_style( 'theme-style', get_template_directory_uri() .'/style.css', array(), "1.1" );

    wp_enqueue_script( 'glide', get_template_directory_uri() .'/assets/js/glide.min.js' );
    wp_enqueue_script( 'main', get_template_directory_uri() .'/assets/js/main.js', array('jquery', 'glide'), "1.0" );
    wp_enqueue_script( 'lightbox', get_template_directory_uri() .'/assets/js/lightbox.js', array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'enqueue_theme_scripts' );

function theme_add_favicon()
{
    ?>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <?php
}
add_action('wp_head', 'theme_add_favicon');
add_action('login_head', 'theme_add_favicon');
add_action('admin_head', 'theme_add_favicon');

/*
 * Upload support for SVG images.
 **/
function theme_add_supported_file_types_to_uploads($file_types) {
    $new_file_types = array();
    $new_file_types['svg'] = 'image/svg+xml';

    $file_types = array_merge($file_types, $new_file_types);
    return $file_types;
}
add_action('upload_mimes', 'theme_add_supported_file_types_to_uploads');

/*
 * Display svg media images as thumbnails.
 **/
function theme_display_svg_media_thumbnails($response, $attachment, $meta) {
    if ($response['type'] === 'image' && $response['subtype'] === 'svg+xml' && class_exists('SimpleXMLElement')) {
        try {
            $path = get_attached_file($attachment->ID);

            if (@file_exists($path)) {

                $svg = new SimpleXMLElement(@file_get_contents($path));
                $src = $response['url'];
                $width = (int) $svg['width'];
                $height = (int) $svg['height'];

                //media gallery
                $response['image'] = compact( 'src', 'width', 'height' );
                $response['thumb'] = compact( 'src', 'width', 'height' );

                //media single
                $response['sizes']['full'] = array(
                    'height'        => $height,
                    'width'         => $width,
                    'url'           => $src,
                    'orientation'   => $height > $width ? 'portrait' : 'landscape',
                );
            }
        } catch(Exception $e) {}
    }

    return $response;
}
add_filter('wp_prepare_attachment_for_js', 'theme_display_svg_media_thumbnails', 10, 3);

function console_log($data) {
    echo '<script>';
    echo 'console.log('. json_encode( $data ) .')';
    echo '</script>';
}

/*
 * Register navigation menus.
 **/
function theme_register_nav_menu() {
    register_nav_menus(array(
        'primary_menu'  => __( 'Pagrindinis Meniu', 'gaschema' ),
        'about_menu'  => __( 'Apie Meniu', 'gaschema' ),
    ));
}
add_action( 'after_setup_theme', 'theme_register_nav_menu', 0 );

function theme_get_menu_slug($name) {
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object($locations[$name]);
    return $menu->slug;
}

/*
 * Add custom option page.
 **/
if ( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Temos Nustatymai',
        'menu_title'	=> 'Temos Nustatymai',
        'menu_slug' 	=> 'theme-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false,
        'icon_url' 		=> 'dashicons-flag',
        'position' 		=> 100
    ));
}

/*
 * Lock plugin version.
 **/
function remove_update_notification($value) {
    unset($value->response["advanced-custom-fields-pro-master/acf.php"]);
    return $value;
}
add_filter('site_transient_update_plugins', 'remove_update_notification');

/*
 * Disable menu item link.
 **/
function theme_filter_nav_menu_objects( $items, $args ) {

    foreach ($items as &$item) {
        $disabled = get_field('meniu-item-disabled', $item);
        if ($disabled) {
            array_push($item->classes, "menu-item-disabled");
        }
    }

    return $items;
}
add_filter('wp_nav_menu_objects', 'theme_filter_nav_menu_objects', 10, 2);

/*
 * Breadcrumbs.
 **/
function get_theme_breadcrumbs()
{
    $breadcrumbs = array();

    if ( is_home() ) {
        array_push($breadcrumbs, get_option('page_for_posts'));

    } else if ( is_page() ) {
        array_push($breadcrumbs, get_the_ID());

    } else if ( is_single() ) {

        array_push($breadcrumbs, get_the_ID());

        if (get_post_type() === "learning") {
            $service_archive = get_pages(array(
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'hierarchical' => 0,
                'meta_value' => 'lessons.php'
            ));

            array_push($breadcrumbs, $service_archive[0]->ID);
        }

        if (get_post_type() === "post") {
            array_push($breadcrumbs, get_option('page_for_posts'));
        }
    }

    array_push($breadcrumbs, get_option( 'page_on_front' ));
    return array_reverse($breadcrumbs);
}

/*
 * Defer scripts.
 **/
function theme_defer_scripts( $tag, $handle, $src ) {

    $defer_scripts = array('jquery-migrate', 'contact-form-7', 'main', 'wp-embed', 'fslightbox', 'glide');
    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
    }

    return $tag;
}
add_filter( 'script_loader_tag', 'theme_defer_scripts', 10, 3 );

/*
 * Preload styles.
 **/
function theme_preload_styles( $tag, $handle, $href, $media ) {

    $defer_scripts = array('style', 'theme-style', 'contact-form-7', 'wp-block-library');
    if ( in_array( $handle, $defer_scripts ) ) {
        return '<link rel="stylesheet preload" as="style" href="' . $href . '" type="text/css" media="'. $media .'" />' . "\n";
    }

    return $tag;
}
add_filter( 'style_loader_tag', 'theme_preload_styles', 10, 4 );

/*
 * Register simple toolbar.
 **/
function simple_toolbar( $toolbars ) {

    $toolbars['Simple' ] = array();
    $toolbars['Simple' ][1] = array('bold' , 'italic' , 'underline', 'strikethrough', 'bullist', 'numlist', 'link', 'undo', 'redo', 'removeformat');

    $toolbars['Simple2' ] = array();
    $toolbars['Simple2' ][1] = array('formatselect', 'bold' , 'italic' , 'underline', 'strikethrough', 'bullist', 'numlist', 'link', 'undo', 'redo', 'removeformat');

    return $toolbars;
}
add_filter( 'acf/fields/wysiwyg/toolbars', 'simple_toolbar' );

/*
 * Polylang unset ACF field translations.
 **/
function unset_cpt_pll( $post_types, $is_settings ) {

    $post_types['acf'] = 'acf';
    return $post_types;
}
add_filter('pll_get_post_types', 'unset_cpt_pll', 10, 2);

/*
 * Copy content from previous post when creating translation post.
 **/
function theme_polylang_lang_editor_title( $title ) {

    // Polylang sets the 'from_post' parameter.
    if ( isset( $_GET['from_post'] ) ) {
        $post = get_post( $_GET['from_post'] );
        if ( $post ) return $post->post_title;
    }

    return $title;
}
add_filter( 'default_title', 'theme_polylang_lang_editor_title' );

function theme_polylang_lang_editor_content( $content ) {

    // Polylang sets the 'from_post' parameter.
    if ( isset( $_GET['from_post'] ) ) {
        $post = get_post( $_GET['from_post'] );
        if ( $post ) return $post->post_content;
    }

    return $content;
}
add_filter( 'default_content', 'theme_polylang_lang_editor_content' );

/*
 * Lazy image handling.
 **/
function theme_lazy_alter_attachment_image($attr) {

    if (isset($attr['srcset'])) unset($attr['srcset']);

    if (isset($attr['class']) ) {
        $attr['class'] = 'lazy-image';
    }

    if (isset($attr['src'])) {

        $attr['data-src'] = $attr['src'];
        unset($attr['src']);
    }

    return $attr;
}
add_filter('wp_get_attachment_image_attributes', 'theme_lazy_alter_attachment_image');

function content_image_lazy_markup($the_content) {

    libxml_use_internal_errors(true);
    $post = new DOMDocument();
    $post->loadHTML('<?xml encoding="utf-8" ?>' . $the_content);
    $images = $post->getElementsByTagName('img');

    foreach( $images as $img ) {

        if( $img->hasAttribute('data-src') ) continue;
        if( $img->parentNode->tagName == 'noscript' ) continue;

        $clone = $img->cloneNode();

        $src = $img->getAttribute('src');
        $img->removeAttribute('src');
        $img->setAttribute('data-src', $src);
        $img->removeAttribute('srcset');

        $imgClass = $img->getAttribute('class');
        $img->setAttribute('class', $imgClass . ' lazy-image');

        $no_script = $post->createElement('noscript');
        $no_script->appendChild($clone);
        $img->parentNode->insertBefore($no_script, $img);
    };

    return $post->saveHTML();
}
add_filter('the_content', 'content_image_lazy_markup');

function theme_display_sidebar_menu() {

    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object($locations['primary_menu']);
    $menu_items = wp_get_nav_menu_items($menu->term_id);

    $menu_id = 0;
    $title = "";
    $has_parent = false;

    foreach ( (array) $menu_items as $key => $menu_item ) :
        if ( $menu_item->object_id == get_the_ID()) :
            $has_parent = true;
            $parent = $menu_item->menu_item_parent;
            $menu_id = $menu_item->db_id;
            $title = $menu_item->object_id;

            foreach ( (array) $menu_items as $key => $menu_item ) :
                if ( $menu_item->db_id == $parent) :
                    $parent = $menu_item->menu_item_parent;
                    $menu_id = $menu_item->db_id;
                    $title = $menu_item->object_id;

                    foreach ( (array) $menu_items as $key => $menu_item ) :
                        if ( $menu_item->db_id == $parent) :
                            $parent = $menu_item->menu_item_parent;
                            $menu_id = $menu_item->db_id;
                            $title = $menu_item->object_id;

                            foreach ( (array) $menu_items as $key => $menu_item ) :
                                if ( $menu_item->db_id == $parent) :
                                    $menu_id = $menu_item->db_id;
                                    $title = $menu_item->object_id;

                                endif;
                            endforeach;

                        endif;
                    endforeach;
                endif;
            endforeach;
        endif;
    endforeach; ?>

    <?php if ( !$has_parent ) return null;?>

    <ul class="level-1">
        <?php foreach ( (array) $menu_items as $key => $menu_item ) :
            if ($menu_item->menu_item_parent == $menu_id ) : ?>
                <li id="level-1-<?php echo $menu_item->object_id; ?>">
                    <?php $parent_id = $menu_item->db_id ?>
                    <a class="<?php echo (get_the_ID() == $menu_item->object_id) ? 'current' : ''; ?>"
                       href="<?php echo $menu_item->url ?>"><?php echo $menu_item->title ?></a>

                    <ul class="level-2">
                        <?php foreach ( (array) $menu_items as $key => $menu_item ) :
                            if ($menu_item->menu_item_parent == $parent_id ) : ?>
                                <?php $parent_id2 = $menu_item->db_id ?>
                                <li id="level-2-<?php echo $menu_item->object_id; ?>">
                                    <a class="<?php echo (get_the_ID() == $menu_item->object_id) ? 'current' : ''; ?>"
                                       href="<?php echo $menu_item->url ?>"><?php echo $menu_item->title ?></a>

                                    <ul class="level-3">
                                        <?php foreach ( (array) $menu_items as $key => $menu_item2 ) :
                                            if ($menu_item2->menu_item_parent == $parent_id2 ) : ?>
                                                <li id="level-3-<?php echo $menu_item2->object_id; ?>">
                                                    <a class="<?php echo (get_the_ID() == $menu_item2->object_id) ? 'current' : ''; ?>"
                                                       href="<?php echo $menu_item2->url ?>"><?php echo $menu_item2->title ?></a>
                                                </li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>

                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>

    <?php
}

/*
 * @action display cookie consent bar if enabled.
 **/
function gdpr_consent_bar() {
    ?> <div id="gdpr-consent"></div> <?php
}
add_action( 'wp_head', 'gdpr_consent_bar');

function gdpr_consent_bar_script() {

    if ( class_exists('acf') ) :

        $message = get_field('gdpr-text-' . ICL_LANGUAGE_CODE, 'option', false);
        $dismiss = get_field('gdpr-cancel-' . ICL_LANGUAGE_CODE, 'option');
        if ($message && $dismiss ) : ?>

            <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js"></script>
            <script async type="text/javascript">
                window.cookieconsent.initialise({
                    container: document.getElementById("gdpr-consent"),
                    onStatusChange: function(status) {},
                    "window":
                        '<div role="dialog" aria-label=”cookieconsent" aria-describedby="cookieconsent:desc" class="cc-window">' +
                        '<span id="cookieconsent:desc" class="cc-message"><?php echo $message; ?></span>' +
                        '<div class="cc-compliance">' +
                        '<a aria-label="dismiss cookie message" role="button" tabindex="0" class="btn cc-btn cc-dismiss"><?php echo $dismiss; ?></a>' +
                        '</div>' +
                        '</div>'
                });
            </script>
        <?php
        endif;
    endif;

}
add_action( 'wp_footer', 'gdpr_consent_bar_script');

function register_acf_gdpr_fields()
{
    $languages = pll_languages_list(array('fields' => 'name'));
    $fields = array();
    for ($i = 0; count($languages) != $i; $i++) {

        $lang = $languages[$i];
        array_push($fields, array(
            'key' => 'gdpr-tab-' . $lang,
            'label' => $lang,
            'name' => 'gdpr-tab-' . $lang,
            'type' => 'tab',
        ));
        array_push($fields, array(
            'key' => 'gdpr-text-' . $lang,
            'label' => 'Text',
            'name' => 'gdpr-text-' . $lang,
            'type' => 'wysiwyg',
            'tabs' => 'all',
            'toolbar' => 'simple',
            'media_upload' => 0,
        ));
        array_push($fields, array(
            'key' => 'gdpr-cancel-' . $lang,
            'label' => 'Consent text',
            'name' => 'gdpr-cancel-' . $lang,
            'type' => 'text',
        ));

    }

    acf_add_local_field_group(array(
        'key' => 'gdpr-fields',
        'title' => 'GDPR',
        'fields' => $fields,
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'theme-settings',
                ),
            ),
        ),
    ));

}

if ( function_exists('pll_the_languages') ) {
    add_action('acf/init', 'register_acf_gdpr_fields');
}