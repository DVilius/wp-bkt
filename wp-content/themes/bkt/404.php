<?php
/**
 * The template for displaying 404 pages (not found).
 */

get_header(); ?>

    <div class="post-container error-404 page-not-found">
        <div class="page-header wrap">

            <h1><?php _e('404', 'bkt'); ?></h1>
            <p><?php esc_html_e( 'Puslapis buvo pašalintas arba perkeltas kitur.', 'bkt' ); ?></p>

        </div>
    </div>

<?php
get_footer();