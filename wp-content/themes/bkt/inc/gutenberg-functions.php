<?php

/*
 * Register gutenberg blocks.
 * */
function theme_acf_register_gutenberg_blocks() {
    if ( function_exists('acf_register_block') ) {
        acf_register_block(array(
            'name'				=> 'carousel',
            'title'				=> __('Carousel', 'bkt'),
            'description'		=> __('Carousel block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/carousel.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'carousel', 'slide' ),
        ));
        acf_register_block(array(
            'name'				=> 'column-block',
            'title'				=> __('Column block', 'bkt'),
            'description'		=> __('Column block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/column-block.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'column' ),
        ));
        acf_register_block(array(
            'name'				=> 'column-block-3',
            'title'				=> __('3 Columns block', 'bkt'),
            'description'		=> __('3 Columns block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/column-block-3.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'column' ),
        ));
        acf_register_block(array(
            'name'				=> 'column-blocks-2',
            'title'				=> __('2 Columns block', 'bkt'),
            'description'		=> __('2 Columns block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/column-block-2.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'column' ),
        ));
        acf_register_block(array(
            'name'				=> 'image-blocks',
            'title'				=> __('Image blocks', 'bkt'),
            'description'		=> __('Image blocks.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/image-blocks.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'block' ),
        ));
        acf_register_block(array(
            'name'				=> 'partners',
            'title'				=> __('Partners', 'bkt'),
            'description'		=> __('Partners block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/partners.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'partners' ),
        ));
        acf_register_block(array(
            'name'				=> 'columns-2',
            'title'				=> __('2 Columns', 'bkt'),
            'description'		=> __('2 Columns block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/columns-2.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'column2' ),
        ));
        acf_register_block(array(
            'name'				=> 'timeline',
            'title'				=> __('Timeline', 'bkt'),
            'description'		=> __('Timeline.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/timeline.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'timeline' ),
        ));
        acf_register_block(array(
            'name'				=> 'pdfs',
            'title'				=> __('PDF', 'bkt'),
            'description'		=> __('PDF block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/pdfs.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'pdf' ),
        ));
        acf_register_block(array(
            'name'				=> 'statistics',
            'title'				=> __('Statistics', 'bkt'),
            'description'		=> __('Statistics block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/statistics.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'statistics' ),
        ));
        acf_register_block(array(
            'name'				=> 'terminal',
            'title'				=> __('Terminal', 'bkt'),
            'description'		=> __('Terminal block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/terminal.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'terminal' ),
        ));
        acf_register_block(array(
            'name'				=> 'contacts',
            'title'				=> __('Contacts', 'bkt'),
            'description'		=> __('Contacts block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/contacts.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'contacts' ),
        ));
        acf_register_block(array(
            'name'				=> 'gallery',
            'title'				=> __('Gallery', 'bkt'),
            'description'		=> __('Gallery block.', 'bkt'),
            'render_template'   => 'template-parts/gutenberg-blocks/gallery.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'gallery' ),
        ));
    }
}
add_action('acf/init', 'theme_acf_register_gutenberg_blocks');

/*
 * Enqueue gutenberg editor styles
 * */
function theme_acf_admin_enqueue_styles() {
    wp_register_style( 'gutenberg-editor', get_stylesheet_directory_uri() . '/editor.css', false, '1.0.0' );
    wp_enqueue_style( 'gutenberg-editor' );
}
add_action( 'acf/input/admin_enqueue_scripts', 'theme_acf_admin_enqueue_styles' );