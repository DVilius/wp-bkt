<?php
/**
 * The template for displaying all pages.
 */
?>

<?php get_header(); ?>

    <div class="page-header">
        <h1 class="title"><?php the_title(); ?></h1>
        <ul class="breadcrumbs">
            <?php foreach (get_theme_breadcrumbs() as $id) : ?>
                <li>
                    <a href="<?php echo get_the_permalink($id) ?>"><?php echo get_the_title($id); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="wrap inner">
        <?php the_content(); ?>

        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <?php foreach ( parse_blocks( get_the_content(null, false, get_option('page_on_front')) ) as $block) :
                    if ($block['blockName'] === "acf/partners"
                        || $block['blockName'] === "acf/gallery" ) echo render_block($block);
                endforeach; ?>

            <?php endwhile; ?>
        <?php endif; ?>
    </div>

<?php get_footer(); ?>