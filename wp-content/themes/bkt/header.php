<?php
/**
 * The header.
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <title><?php echo wp_title("", false); ?></title>
    <?php if (get_bloginfo('description') != '') : ?>
        <meta name="description" content="<?php bloginfo('description'); ?>" />
    <?php endif; ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-171507841-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-171507841-1');
    </script>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php
$slug = null;
if ( is_page() ) {
    $slug = get_queried_object()->post_name;

    if (function_exists('pll_get_post'))
    {
        $pageID = pll_get_post(get_the_ID(), 'lt');
        $slug = get_post_field( 'post_name', $pageID );
    }

}
?>

<div id="page" class="hfeed site<?php echo !is_front_page() ? ' inner-page' : ''?><?php echo $slug ? ' ' . $slug : ''?>">
    <div class="header <?php echo is_front_page() ? 'front-header' : 'inner-header'; ?>">

        <div class="mobile-navigation">

            <img width="150" height="45" src="<?php echo get_template_directory_uri() . '/assets/images/logo-white.svg'; ?>" alt="">
            <div class="close-menu" id="close-menu"></div>

            <div class="mobile-content">
                <?php wp_nav_menu( array(
                    'depth'             => 1,
                    'theme_location'    => 'primary_menu',
                    'container_class'   => 'primary-menu',
                )); ?>

                <div class="mobile-contacts">
                    <a class="phone" href="tel:<?php the_field('phone', 'option'); ?>">
                        <strong><?php the_field('phone', 'option'); ?></strong>
                    </a>
                    <a class="mail" href="mailto:<?php the_field('email', 'option'); ?>">
                        <?php the_field('email', 'option'); ?>
                    </a>
                    <?php if (ICL_LANGUAGE_CODE == 'ru') : ?>
                        <a class="address" target="_blank" href="https://www.google.com/maps/place/<?php the_field('address_ru', 'option'); ?>"><?php the_field('address_ru', 'option'); ?></a>
                    <?php else : ?>
                        <a class="address" target="_blank" href="https://www.google.com/maps/place/<?php the_field('address', 'option'); ?>"><?php the_field('address', 'option'); ?></a>
                    <?php endif; ?>
                </div>
            </div>

        </div>

        <div class="wrap">

            <div class="logo-wrapper">
                <a href="<?php echo get_home_url(); ?>" title="<?php _e('Birių krovinių terminalas', 'bkt'); ?>">
                    <img width="206" height="91" src="<?php echo get_template_directory_uri() . '/assets/images/logo.svg'; ?>" alt="">
                    <h5 class="logo-text"><?php _e('Birių krovinių terminalas', 'bkt'); ?></h5>
                </a>
            </div>

            <div class="navigation">
                <?php wp_nav_menu( array(
                    'depth'             => 1,
                    'theme_location'    => 'primary_menu',
                    'container_class'   => 'primary-menu',
                )); ?>

                <div class="mobile-menu" id="mobile-menu"></div>
            </div>

            <div class="contacts">
                <a class="phone" href="tel:<?php the_field('phone', 'option'); ?>"></a>
                <a class="mail" href="mailto:<?php the_field('email', 'option'); ?>"></a>
            </div>

            <div class="lang">
                <?php pll_the_languages( array( 'dropdown' => 1, 'display_names_as' => 'slug' ) ); ?>
            </div>

        </div>
    </div>