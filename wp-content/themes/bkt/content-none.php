<?php
/**
 * The template part for displaying a message that posts cannot be found.
 */
?>

<div class="no-results not-found wrap">
    <?php if ( is_search() ) : ?>

        <h1><?php esc_html_e( 'Dėja, nieko nerasta', 'bkt' ); ?></h1>
        <p><?php esc_html_e( 'Pagal įvestus raktažodžius rezultatų nerasta.', 'bkt' ); ?></p>

    <?php else : ?>

        <h1><?php esc_html_e( 'Dėja, toks puslapis nerastas', 'bkt' ); ?></h1>
        <p><?php esc_html_e( 'Puslapis buvo pašalintas arba perkeltas kitur.', 'bkt' ); ?></p>

    <?php endif; ?>
</div>