<?php
/**
 * The template for displaying search results.
 */

get_header(); ?>

	<div class="page-wrap wrap">
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php printf(esc_attr__( 'Paieškos rezultatai pagal raktažodį: %s', 'bkt' ),
                        '<span>' . get_search_query() . '</span>'
                    ); ?>
				</h1>
			</header>

            <?php while ( have_posts() ) : the_post(); ?>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            <?php endwhile; ?>

		<?php else :
			get_template_part( 'content', 'none' );
		endif;
		?>
	</div>

<?php
get_footer();