'use strict';

var gulpfile = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

var themePath = './wp-content/themes/bkt';
var sassPath = themePath + '/assets/scss';

gulpfile.task('styles', gulpfile.series(function() {
    var options = {
        outputStyle: 'compressed'
    };

    return gulpfile
        .src(sassPath + '/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass(options).on('error', sass.logError))
        .pipe(sourcemaps.write('/'))
        .pipe(gulpfile.dest(themePath));
}));

gulpfile.task('watch', gulpfile.series(function() {
    gulpfile.watch(sassPath + '/**/*.scss', gulpfile.series('styles'));
}));

gulpfile.task('default', gulpfile.series('watch'));